/* This is the implementation file for the queue, where you will define how the
* abstract functionality (described in the header file interface) is going to
* be accomplished in terms of explicit sequences of C++ statements. */

#include "queue.h"
#include <stdio.h>

/*Note./ that we will use the following "class invariants" for our data members:
1. frontIndx is the index of the element at the front of the queue, if any.
2. nextBackIndx is the index that is one after the back of the queue.
3. the elements data[frontIndx...nextBackIndx-1] are the valid elements
of the queue, where each index in the range [frontIndx...nextBackIndx-1]
is reduced modulo qArraySize ("modulo" === remainder === %-operator in C)
4. the empty queue will be represented by frontIndx == nextBackIndx
As a result of invariant 4, we must have that the queue is full when
nextBackIndx is one away from frontIndx: otherwise an insertion will make
the queue appear to be empty.  Note that "away from" in the preceding sentence
doesn't necessarily mean "one less" due to the circular way we use the array.*/



// I used a counter in which I modified the header file. If it is wrong, then I'll accept.
namespace csc212
{
	queue::queue()
	{
		/* initialize the queue to be empty. */
		this->nextBackIndx = 0;
		this->frontIndx = 0;
		this->counter = 0;
	}

	queue::~queue()
	{
		/* since we did not dynamically allocate any memory,
		* there is nothing for us to release here.  */
	}

	unsigned long queue::getBack()
	{
		if (this->nextBackIndx != this->frontIndx)
			return this->data[this->nextBackIndx - 1];
		return 0;
	}
	unsigned long queue::getFront()
	{
		return this->data[this->frontIndx];
	}
	unsigned long queue::getCapacity()
	{
		return csc212::qArraySize - 1;
	}

	unsigned long queue::getSize()
	{
		return counter;
	}

	bool queue::isEmpty()
	{
		return (this->nextBackIndx == this->frontIndx);
	}

	bool queue::isFull()
	{
		return ((this->nextBackIndx) + 1 == this->frontIndx);
	}

	bool queue::push(unsigned long v)
	{
		this->data[nextBackIndx] = v;
		if (((this->nextBackIndx) + 1 != this->frontIndx) && ((this->nextBackIndx) + 1 > qArraySize)) {
			this->nextBackIndx = ((this->nextBackIndx) + 1) % (qArraySize - 1);
			counter++;
			return true;
		}
		else if ((this->nextBackIndx) + 1 <= (qArraySize - 1)) {
			this->nextBackIndx = (this->nextBackIndx) + 1;
			counter++;
			return true;
		}
		else
		{
			return false;
		}
	}

	unsigned long queue::pop()
	{
		unsigned long temp = this->data[frontIndx]; // Not sure if this is right. Hope so.
		if ((this->frontIndx) + 1 > qArraySize) {
			this->frontIndx = ((this->frontIndx) + 1) % (qArraySize - 1);
			this->nextBackIndx = ((this->nextBackIndx) + 1);
			counter--;
			return temp;
		}
		else {
			this->frontIndx = (this->frontIndx) + 1;
			counter--;
			return temp;
		}

		return 0;
	}

	void queue::erase()
	{
		for (unsigned int i = 0; i < qArraySize; i++)
			this->data[i] = 0;

		this->frontIndx = 0;		// Assuming we're supposed to reset the queue.
		this->nextBackIndx = 0;
	}

	ostream& operator<<(ostream& o, const queue& q)
	{
		unsigned long i;
		for (i = q.frontIndx; i != q.nextBackIndx; i = (i + 1) % csc212::qArraySize)
			o << q.data[i] << "  ";
		return o;
	}
	void queue::print(FILE* f)
	{
		for (unsigned long i = this->frontIndx; i != this->nextBackIndx;
		i = (i + 1) % csc212::qArraySize)
			fprintf(f, "%lu ", this->data[i]);
		fprintf(f, "\n");
	}
}
